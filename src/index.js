const {
  BaseKonnector,
  requestFactory,
  scrape,
  log
} = require('cozy-konnector-libs')
const request = requestFactory({
  // The debug mode shows all the details about HTTP requests and responses. Very useful for
  // debugging but very verbose. This is why it is commented out by default
  debug: false,
  // Activates [cheerio](https://cheerio.js.org/) parsing on each page
  cheerio: true,
  // If cheerio is activated do not forget to deactivate json parsing (which is activated by
  // default in cozy-konnector-libs
  json: false,
  // This allows request-promise to keep cookies between requests
  jar: true
})

const VENDOR = 'cerballiance'
const baseUrl = 'https://espacepatient.cerballiance.fr'

module.exports = new BaseKonnector(start)

// The start function is run by the BaseKonnector instance only when it got all the account
// information (fields). When you run this connector yourself in "standalone" mode or "dev" mode,
// the account information come from ./konnector-dev-config.json file
// cozyParameters are static parameters, independents from the account. Most often, it can be a
// secret api key.
async function start(fields, cozyParameters) {
  log('info', 'Authenticating ...')
  if (cozyParameters) log('debug', 'Found COZY_PARAMETERS')
  await authenticate(fields.login, fields.password)
  log('info', 'Successfully logged in')
  // The BaseKonnector instance expects a Promise as return of the function
  log('info', 'Fetching the list of documents')
  const $ = await request(`${baseUrl}/action/medical`)
  // cheerio (https://cheerio.js.org/) uses the same api as jQuery (http://jquery.com/)
  log('info', 'Parsing list of documents')
  const documents = await parseDocuments($, fields.login)

  // Here we use the saveBills function even if what we fetch are not bills,
  // but this is the most common case in connectors
  log('info', 'Saving data to Cozy')
  await this.saveFiles(documents, fields.folderPath, {
    // This is a bank identifier which will be used to link bills to bank operations. These
    // identifiers should be at least a word found in the title of a bank operation related to this
    // bill. It is not case sensitive.
    identifiers: ['Cerballiance'],
    keys: ['vendorRef']
  })
}

// This shows authentication using the [signin function](https://github.com/konnectors/libs/blob/master/packages/cozy-konnector-libs/docs/api.md#module_signin)
// even if this in another domain here, but it works as an example
async function authenticate(username, password) {
  const login = await request(`${baseUrl}/action/login`)
  const csrf = login('input[name="_csrf"]').attr('value')
  const loginForm = {
    username: username,
    password: password,
    _csrf: csrf
  }
  const res = await request.post(`${baseUrl}/action/login`, {
    form: loginForm,
    resolveWithFullResponse: true,
    simple: false
  })
  if (res.req.path != '/action/home') {
    throw new Error('LOGIN_FAILED')
  }
}

// The goal of this function is to parse a HTML page wrapped by a cheerio instance
// and return an array of JS objects which will be saved to the cozy by saveBills
// (https://github.com/konnectors/libs/blob/master/packages/cozy-konnector-libs/docs/api.md#savebills)
function parseDocuments($, login) {
  // You can find documentation about the scrape function here:
  // https://github.com/konnectors/libs/blob/master/packages/cozy-konnector-libs/docs/api.md#scrape
  const refContract = login
  const docs = scrape(
    $,
    {
      date: {
        sel: 'td:nth-child(3)',
        parse: text => text.match(/[0-9]{2}\/[0-9]{2}\/[0-9]{4}/)
      },
      doctor: {
        sel: 'td:nth-child(3)',
        parse: text => text.match(/prescrit par (.*)/)[1]
      },
      vendorRef: {
        sel: 'td:nth-child(2)'
      },
      fileurl: {
        sel: 'td:nth-child(4) a',
        attr: 'href'
      }
    },
    'table#medicals tbody tr'
  )
  return docs.map(result => {
    const date = normalizeDate(result.date[0])
    var data = {
      ...result,
      refContract,
      filename: `${formatDate(date)}_${result.doctor}_${result.vendorRef}.pdf`,
      date: date,
      vendor: VENDOR,
      vendorRef: result.vendorRef,
      fileAttributes: {
        metadata: {
          carbonCopy: true,
          caterogies: ['health']
        }
      }
    }
    if (result.billPath) {
      data['fileurl'] = `${result.billPath}`
    }
    return data
  })
}

function normalizeDate(date) {
  const [day, month, year] = date.split('/')
  return new Date(`${year}-${month}-${day}`)
}

// Return a string representation of the date that follows this format:
// "YYYY-MM-DD". Leading "0" for the day and the month are added if needed.
function formatDate(date) {
  let month = date.getMonth() + 1
  if (month < 10) {
    month = '0' + month
  }

  let day = date.getDate()
  if (day < 10) {
    day = '0' + day
  }

  let year = date.getFullYear()

  return `${year}${month}${day}`
}
